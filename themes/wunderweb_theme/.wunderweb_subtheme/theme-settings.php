<?php
/**
 * @file
 * theme-settings.php
 *
 * Provides theme settings for wunderweb based themes when admin theme is not.
 *
 * @see ./includes/settings.inc
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Implements hook_form_FORM_ID_alter().
 */
function wunderweb_subtheme_form_system_theme_settings_alter(&$form, FormStateInterface $form_state, $form_id = NULL) {

}
