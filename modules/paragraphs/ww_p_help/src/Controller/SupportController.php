<?php

namespace Drupal\ww_p_help\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Controller routines for help routes.
 */
class SupportController extends ControllerBase {

  /**
   * Prints a page about wunderweb distribution.
   *
   * @return array
   *   A render array for the info page.
   */
  public function render() {
    $output = '<h3>' . t('wunderweb is a Drupal 8 profile designed to kickstart a new webpage in a few minutes') . '</h3>';
    $output .= '<p>' . t('More info about wunderweb - <a href=":link">See official wunderweb website</a>.', [':link' => 'https://wunderweb.com/']) . '</p>';
    $output .= '<h3>' . t('Support') . '</h3>';
    $output .= '<p>' . t('Do You need support with wunderweb? -  <a href=":link">wunderweb.com</a>.', [':link' => 'https://wunderweb.com']) . '</p>';
    $output .= '<h3>' . t('Github') . '</h3>';
    $output .= '<p>' . t('<a href=":link">https://github.com/wunderweb/wunderweb_project</a> - Boilerplate for new projects based on wunderweb. If you wish to use wunderweb - fork (or download) this repository. It contains a minimum set of code to start your new website.', [':link' => ' https://github.com/wunderweb/wunderweb_project']) . '</p>';
    $output .= '<p>' . t('<a href=":link">https://github.com/wunderweb/wunderweb</a> - This is Drupal installation profile.', [':link' => ' https://github.com/wunderweb/wunderweb_project']) . '</p>';
    return [
      '#type' => 'markup',
      '#markup' => '<div class="container">' . $output . '</div>',
    ];
  }
}
