/**
 * @file
 * The script that activates Slick sliders.
 */

(function ($) {
  'use strict';

  Drupal.behaviors.ww_p_slider = {


    attach: function (context, settings) {

      $('.ww-p-slider:not(.slick-initialized)').slick({
        infinite: true,
        slidesToScroll: 1,
        swipeToSlide: true,
        touchMove: true,
        autoplay: false,
        // slide: $(this).find('.ww-p-banner'),
        dots: true,
        lazyload: 'ondemand',
      });

    }
  };
})(jQuery);
