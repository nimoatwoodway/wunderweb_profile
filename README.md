# wunderweb profile for Drupal 8 #

## Favicons ##
Generate Favicons from https://www.favicon-generator.org
And save them in /web/themes/custom/wunderweb_subtheme/favicons

## What is wunderweb? ##
wunderweb is a Drupal 8 profile designed to kickstart a new webpage in a few minutes. It's based on the latest frontend technologies, including Bootstrap 4. The maintainer of wunderweb is [wunderwerk.io](https://wunderwerk.io).

* **Official website**: [wunderwerk.io](https://wunderwerk.io)

## What's in this repository? ##
This repository contains a Drupal profile. When you put it in your /profiles directory, the Drupal installer gets modified and installs base wunderweb theme, some module dependencies, and demo content.

## Change Google API Key##
Go to https://console.cloud.google.com and create a key with Geocoding API and Maps JavaScript API enabled. And add it on: /admin/config/system/geofield_map_settings